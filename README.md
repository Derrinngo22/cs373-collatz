# CS373: Software Engineering Collatz Repo

* Name: Derrin Ngo

* EID: dn7854

* GitLab ID: derrinngo22

* HackerRank ID: derrinngo

* Git SHA: e949a013d8d3553791129804b92f01ba3df3b45d

* GitLab Pipelines: https://gitlab.com/Derrinngo22/cs373-collatz/pipelines

* Estimated completion time: 10

* Actual completion time: 8

* Comments: I enjoyed learning how to use all the tools for this project. I feel 
            like they are really useful for helping me stay organized.