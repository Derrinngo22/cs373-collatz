#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------
cache = {1: 1}  # lazy cache!


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    if j < i:
        i, j = j, i
    max_c = 1
    half_entries = j // 2 + 1
    if i < half_entries:  # cuts the number of integers to check in half if applicable
        i = (j // 2) + 1
    for x in range(i, j + 1):
        c = 1  # reset cycle length
        if x in cache:  # check if in the cache
            c = cache[x]
        else:  # if not in cache find cycle length
            c = collatz_equation(x)
            cache[x] = c
        if c > max_c:
            max_c = c
    assert max_c > 0
    return max_c


def collatz_equation(n: int) -> int:
    """
    A seperate function that takes in a int n as a parameter.
    This function finds the cycle length of the given int
    """
    c = 0
    while n > 1:
        if (n % 2) == 0:
            n = n // 2
            c += 1
            if n in cache:  # check if this number is in the cache
                return c + cache[n]
        else:
            n = n + (n >> 1) + 1  # optimization
            c += 2
            if n in cache:  # check if this number is in the cache
                return c + cache[n]
    return c

# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
